﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRLracing.Model;
using System.Windows.Forms;

namespace PRLracing.Controller
{
    public class Race
    {
        /// <summary>
        /// Połączenie z bazą danych
        /// </summary>
        private DatabaseDataContext context;

        /// <summary>
        /// ID pojazdu przeciwnika
        /// </summary>
        private int oponentId = 0;

        /// <summary>
        /// Generetor liczb losowych
        /// </summary>
        Random rand = new Random();

        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="paramContext"></param>
        public Race(DatabaseDataContext paramContext)
        {
            context = paramContext;
        }

        /// <summary>
        /// Wyścig!
        /// </summary>
        public bool Go(string opponent = null)
        {
            if(GameUser.userCars[GameUser.selectedCar].damage > 90)
            {
                MessageBox.Show("Auto jest zbyt uszkodzone, zmień je lub napraw.", "Błąd");
                return false;
            }
            if (!OpponentExists(opponent)) 
                return false;
            int userPoints = CalculateUserPoints();
            int opponentPoints = CalculateOpponentPoints();
            bool win = (rand.Next(userPoints + opponentPoints) > opponentPoints);
            ModifyAwards(win, userPoints, opponentPoints);
            if (win)
            {
                MessageBox.Show("Gratulacje! Wygrałeś!", "Zwycięstwo");
            }
            else
            {
                MessageBox.Show("A niech to! Przegrałeś!", "Porażka");
            }
            return true;
        }

        /// <summary>
        /// Przyznanie nagród za wyścig
        /// </summary>
        /// <param name="win"></param>
        /// <param name="userPoints"></param>
        /// <param name="opponentPoints"></param>
        private void ModifyAwards(bool win, int userPoints, int opponentPoints)
        {
            UserCar opponentCar = (from c in context.UserCars
                           where c.ID == oponentId
                           select c).FirstOrDefault();
            UserCar userCar = (from c in context.UserCars
                               where c.ID == GameUser.userCars[GameUser.selectedCar].id
                               select c).FirstOrDefault();
            User opponent = (from u in context.Users
                             where u.ID == opponentCar.UserID
                                  select u).FirstOrDefault();
            User user = context.Users.Where(u => u.ID == GameUser.userID).FirstOrDefault();
            if(win)
            {
                user.Points += 10;
                int money = (int)opponent.Money * opponentPoints / (opponentPoints + userPoints);
                opponent.Money -= money;
                user.Money += money;
            }
            else
            {
                opponent.Points += 10;
                int money = (int)user.Money * userPoints / (opponentPoints + userPoints);
                opponent.Money += money;
                user.Money -= money;
            }
            opponentCar.Damage += rand.Next(1, 10);
            userCar.Damage += rand.Next(1, 10);
            context.SubmitChanges();
        }

        /// <summary>
        /// Wybór przeciwnika (auta)
        /// </summary>
        /// <param name="opponent"></param>
        /// <returns></returns>
        private bool OpponentExists(string opponent)
        {
            if(opponent != null)
            {
                var user = context.Users.Where(u => u.Name == opponent);
                if (user.Count() != 1)
                {
                    MessageBox.Show("Nie ma takiego przeciwnika!", "Błąd");
                    return false;
                }
                int opponentUserID = user.FirstOrDefault().ID;
                var userCars = context.UserCars.Where(c => c.UserID == opponentUserID && c.Damage < 80);
                if (userCars.Count() < 1)
                {
                    MessageBox.Show("Przeciwnik nie ma sprawnych pojazdów!", "Błąd");
                    return false;
                }
                oponentId = userCars.ElementAt(rand.Next(userCars.Count())).ID;
                return true;
            }
            var users = context.UserCars.Where(c => c.UserID != GameUser.userID && c.Damage < 80);
            if(users.Count() < 1)
            {
                MessageBox.Show("Nie ma dostępnych przeciwników!", "Błąd");
                return false;
            }
            oponentId = users.ToList().ElementAt(rand.Next(users.Count())).ID;
            return true;
        }

        /// <summary>
        /// Obliczenie punktów dla użytkownika
        /// </summary>
        /// <returns></returns>
        private int CalculateUserPoints()
        {
            int points = GameUser.points;
            points += Car.newCars[GameUser.userCars[GameUser.selectedCar].model].handling;
            points += Car.newCars[GameUser.userCars[GameUser.selectedCar].model].power;
            points += Car.newCars[GameUser.userCars[GameUser.selectedCar].model].breaking;
            points += GameUser.userCars[GameUser.selectedCar].tuningLevel * 15;
            return points;
        }

        /// <summary>
        /// Obliczenie punktów dla przeciwnika
        /// </summary>
        /// <returns></returns>
        private int CalculateOpponentPoints()
        {
            UserCar car = (from c in context.UserCars
                           where c.ID == oponentId
                           select c).FirstOrDefault();
            int opponentPoints = (from u in context.Users
                                  where u.ID == car.UserID
                                  select u.Points).FirstOrDefault();
            int points = opponentPoints;
            points += Car.newCars[car.Model].handling;
            points += Car.newCars[car.Model].power;
            points += Car.newCars[car.Model].breaking;
            points += car.TuningLevel * 15;
            return points;
        }
    }
}
