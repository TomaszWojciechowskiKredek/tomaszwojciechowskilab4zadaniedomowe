﻿using PRLracing.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRLracing.Model
{
    public static class Car
    {
        /// <summary>
        /// Struktura przechowująca informacje o aucie
        /// </summary>
        public struct CarStruct
        {
            public string name;
            public int price;
            public int handling;
            public int breaking;
            public int power;
            public Image image;
        }

        /// <summary>
        /// Lista wszystkich dostępnych pojazdów
        /// </summary>
        public static readonly CarStruct[] newCars = { new CarStruct() { name = "Mikrus", price = 25000, breaking = 10, handling = 15, power = 10, image = Resources.mikrus },
                                                new CarStruct() { name = "Fiat 126p", price = 30000, breaking = 10, handling = 15, power = 15, image = Resources.fiat126p },
                                                new CarStruct() { name = "Nysa", price = 45000, breaking = 15, handling = 20, power = 20, image = Resources.nysa },
                                                new CarStruct() { name = "Żuk", price = 50000, breaking = 20, handling = 20, power = 20, image = Resources.zuk },
                                                new CarStruct() { name = "Trabant", price = 65000, breaking = 30, handling = 20, power = 25, image = Resources.trabant },
                                                new CarStruct() { name = "Warszawa", price = 80000, breaking = 30, handling = 30, power = 40, image = Resources.warszawa },
                                                new CarStruct() { name = "Skoda 110", price = 85000, breaking = 40, handling = 30, power = 45, image = Resources.skoda110 },
                                                new CarStruct() { name = "Fiat 125p", price = 130000, breaking = 50, handling = 40, power = 45, image = Resources.fiat125p },
                                                new CarStruct() { name = "Polonez", price = 150000, breaking = 45, handling = 40, power = 60, image = Resources.polonez },
                                                new CarStruct() { name = "Syrena Sport", price = 300000, breaking = 70, handling = 80, power = 80, image = Resources.syrenasport } };

        /// <summary>
        /// Zwraca listę wszystkich dostępnych samochodów
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllCars()
        {
            var output = from car in newCars
                         select car.name;
            return output.ToList();
        }
    }
}
