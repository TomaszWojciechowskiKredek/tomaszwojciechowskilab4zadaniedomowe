﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRLracing.Model
{
    public static class GameUser
    {
        /// <summary>
        /// Struktura dla auta użytkownika
        /// </summary>
        public struct UserCarStruct
        {
            public int id;
            public int model;
            public int tuningLevel;
            public int damage;
        }

        /// <summary>
        /// ID użytkownika
        /// </summary>
        public static int userID = 0;

        /// <summary>
        /// Nazwa użytkownika
        /// </summary>
        public static string username = string.Empty;

        /// <summary>
        /// Liczba punktów
        /// </summary>
        public static int points = 0;

        /// <summary>
        /// Konto pieniężne
        /// </summary>
        public static float money = 0;

        /// <summary>
        /// Samochody użytkownika
        /// </summary>
        public static List<UserCarStruct> userCars = new List<UserCarStruct>();

        /// <summary>
        /// Wybrany samochód, który aktualnie prowadzisz
        /// </summary>
        public static int selectedCar = 0;

        /// <summary>
        /// Aktualizacja danych
        /// </summary>
        /// <param name="context"></param>
        public static void Update(DatabaseDataContext context)
        {
            var user = (from u in context.Users
                        select u).Where(x => x.Name == username).First();
            GameUser.money = (float)user.Money;
            GameUser.points = user.Points;
            GameUser.userID = user.ID;
            var cars = context.UserCars.Where(c => c.UserID == GameUser.userID).ToList();
            userCars.Clear();
            foreach (var item in cars)
            {
                UserCarStruct car = new UserCarStruct() { id = item.ID, model = item.Model, tuningLevel = item.TuningLevel, damage = item.Damage };
                userCars.Add(car);
            }
        }


    }
}
