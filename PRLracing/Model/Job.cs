﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRLracing.Model
{
    public static class Job
    {
        /// <summary>
        /// Struktura przchowująca informacje o pracy
        /// </summary>
        public struct JobStruct
        {
            public string name;
            public TimeSpan duration;
            public int salary;
        }

        /// <summary>
        /// Lista wszystkich prac
        /// </summary>
        public static readonly JobStruct[] allJobs = { new JobStruct() { name = "Rozdawanie ulotek", duration = new TimeSpan(0, 0, 5, 0), salary = 500 },
                                                       new JobStruct() { name = "Rozwożenie pizzy", duration = new TimeSpan(0, 0, 10, 0), salary = 1200 },
                                                       new JobStruct() { name = "Kierowanie taksówką", duration = new TimeSpan(0, 1, 0, 0), salary = 7000 },
                                                       new JobStruct() { name = "Kierowca ciężarówki", duration = new TimeSpan(0, 5, 0, 0), salary = 20000 },
                                                       new JobStruct() { name = "Transpot międzynarodowy", duration = new TimeSpan(0, 12, 0, 0), salary = 35000 },
                                                       new JobStruct() { name = "Czasowy zastępca managera firmy transportowej", duration = new TimeSpan(1, 0, 0, 0), salary = 50000 } };
        
        /// <summary>
        /// Lista wszystkich prac (string)
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllJobs()
        {
            var output = from job in allJobs
                         select job.name;
            return output.ToList();
        }
    }
}
