﻿using PRLracing.Controller;
using PRLracing.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRLracing.View
{
    public partial class FormMain : Form
    {
        #region Pola klasy

        /// <summary>
        /// Informacja zwrotna czy zamknąć program, czy przelogować
        /// </summary>
        public bool finish = true;

        /// <summary>
        /// Połączenie z bazą danych
        /// </summary>
        DatabaseDataContext context;

        Race race;

        #endregion

        #region Konstruktor

        public FormMain(DatabaseDataContext paramContext)
        {
            InitializeComponent();
            context = paramContext;
            var jobs = context.UserJobs.Where(j => j.userID == GameUser.userID).OrderBy(o => o.finishTime);
            if(jobs.Count() > 1)
            {
                context.UserJobs.DeleteAllOnSubmit(jobs);
            }
            else if (jobs.Count() == 1 && jobs.First().finishTime > DateTime.Now)
            {
                MessageBox.Show("Jesteś w pracy, wróć później.", "Pracuj!");
                finish = false;
                Close();
            }
            else if (jobs.Count() == 1)
            {
                context.Users.Where(u => u.ID == GameUser.userID).First().Money += jobs.First().salary;
                context.UserJobs.DeleteAllOnSubmit(jobs);
            }
            context.SubmitChanges();
            GameUser.Update(context);
            UpdateView();
            listBoxNewCar.DataSource = Car.GetAllCars();
            listBoxJobs.DataSource = Job.GetAllJobs();
            dataGridViewBestRacers.DataSource = (from u in context.Users
                                                 orderby u.Points descending
                                                 select new { Użytkownik = u.Name, LiczbaPunktów = u.Points }).Take(100).ToList();
            race = new Race(context);
        }

        #endregion

        #region Wspólne elementy okna

        /// <summary>
        /// Aktualizacja wyświetlanych danych
        /// </summary>
        private void UpdateView()
        {
            labelUsername.Text = GameUser.username;
            labelPoints.Text = GameUser.points + " XP";
            labelMoney.Text = GameUser.money.ToString("C");
            listBoxCar.DataSource = (from c in GameUser.userCars
                                     select new { name = Car.newCars[c.model].name, id = c.id }).ToList();
            listBoxCar.DisplayMember = "name";
        }

        /// <summary>
        /// Wylogowanie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLogout_Click(object sender, EventArgs e)
        {
            finish = false;
            Close();
        }

        /// <summary>
        /// Wyjście z programu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonQuit_Click(object sender, EventArgs e)
        {
            finish = true;
            Close();
        }

        #endregion

        #region Zakładka komisu

        /// <summary>
        /// Aktualizowanie okna komisu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxNewCar_SelectedIndexChanged(object sender, EventArgs e)
        {
            Car.CarStruct car = Car.newCars.Where(c => c.name == listBoxNewCar.SelectedValue.ToString()).First();
            labelHandling.Text = "Prowadzenie: " + car.handling.ToString();
            labelPower.Text = "Moc: " + car.power.ToString();
            labelBreaking.Text = "Hamowanie: " + car.breaking.ToString();
            labelPrice.Text = "Cena: " + car.price.ToString("C");
            pictureBoxNewCar.Image = car.image;
            if (GameUser.money < car.price)
                buttonBuyNewCar.Enabled = false;
            else
                buttonBuyNewCar.Enabled = true;
        }

        /// <summary>
        /// Potwierdzenie zakupu auta
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBuyNewCar_Click(object sender, EventArgs e)
        {
            int price = Car.newCars[listBoxNewCar.SelectedIndex].price;
            if (GameUser.money < price)
            {
                MessageBox.Show("Brakuje Ci pięniędzy.", "Błąd!");
                return;
            }
            DialogResult result = MessageBox.Show("Czy na pewno chcesz kupić to auto za " + price.ToString("C") + "?", "Potwierdzenie", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;
            UserCar toInsert = new UserCar() { UserID = GameUser.userID, TuningLevel = 0, Model = listBoxNewCar.SelectedIndex, Damage = 0 };
            context.UserCars.InsertOnSubmit(toInsert);
            context.Users.Where(u => u.ID == GameUser.userID).First().Money -= price;
            context.SubmitChanges();
            GameUser.Update(context);
            UpdateView();
            GameUser.selectedCar = 0;
        }

        #endregion

        #region Zakładka Twoje samochody

        private void listBoxCar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxCar.SelectedValue != null)
            {
                labelUserCarHandling.Text = "Prowadzenie: " + (Car.newCars[GameUser.userCars[listBoxCar.SelectedIndex].model].handling + GameUser.userCars[listBoxCar.SelectedIndex].tuningLevel * 5).ToString();
                labelUserCarPower.Text = "Moc: " + (Car.newCars[GameUser.userCars[listBoxCar.SelectedIndex].model].power + GameUser.userCars[listBoxCar.SelectedIndex].tuningLevel * 5).ToString();
                labelUserCarBreaking.Text = "Hamowanie: " + (Car.newCars[GameUser.userCars[listBoxCar.SelectedIndex].model].breaking + GameUser.userCars[listBoxCar.SelectedIndex].tuningLevel * 5).ToString();
                labelUserCarDamage.Text = "Uszkodzenia: " + GameUser.userCars[listBoxCar.SelectedIndex].damage + "%";
                pictureBoxCar.Image = Car.newCars[GameUser.userCars[listBoxCar.SelectedIndex].model].image;
            }
        }

        /// <summary>
        /// Wybór aktywnego pojazdu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDefault_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Usiadłeś za kółkiem: " + Car.newCars[GameUser.userCars[listBoxCar.SelectedIndex].model].name, "Szerokiej drogi!");
            GameUser.selectedCar = listBoxCar.SelectedIndex;
        }

        private void buttonSell_Click(object sender, EventArgs e)
        {
            if (GameUser.userCars.Count < 2)
            {
                MessageBox.Show("Nie możesz sprzedać swojego jedynego samochodu.", "Ostrzeżenie!");
                return;
            }
            int value = Car.newCars[GameUser.userCars[listBoxCar.SelectedIndex].model].price / 2 + GameUser.userCars[listBoxCar.SelectedIndex].tuningLevel * 5000;
            DialogResult result = MessageBox.Show("Czy na pewno chcesz sprzedać to auto za " + value.ToString("C") + "?", "Potwierdzenie", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;
            var toDelete = context.UserCars.Where(c => c.ID == GameUser.userCars[listBoxCar.SelectedIndex].id).First();
            context.UserCars.DeleteOnSubmit(toDelete);
            context.Users.Where(u => u.ID == GameUser.userID).First().Money += value;
            context.SubmitChanges();
            GameUser.Update(context);
            UpdateView();
            GameUser.selectedCar = 0;
        }

        /// <summary>
        /// Tuning, poprawianie osiągów samochodu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonTune_Click(object sender, EventArgs e)
        {
            int price = 10000;
            if (GameUser.money < price)
            {
                MessageBox.Show("Brakuje Ci pięniędzy na ulepszenie pojazdu.", "Błąd!");
                return;
            }
            DialogResult result = MessageBox.Show("Czy na pewno chcesz poprawiść osiągi wybranego auta za cenę " + price.ToString("C") + "?", "Potwierdzenie", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;
            context.UserCars.Where(c => c.ID == GameUser.userCars[listBoxCar.SelectedIndex].id).First().TuningLevel++;
            context.Users.Where(u => u.ID == GameUser.userID).First().Money -= price;
            context.SubmitChanges();
            GameUser.Update(context);
            UpdateView();
        }

        /// <summary>
        /// Naprawianie auta uszkodzonego podczas wyścigu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRepair_Click(object sender, EventArgs e)
        {
            if (GameUser.userCars[listBoxCar.SelectedIndex].damage == 0)
            {
                MessageBox.Show("To auto nie jest uszkodzone.", "Nie stresuj się!");
                return;
            }
            int price = Car.newCars[GameUser.userCars[listBoxCar.SelectedIndex].model].price / 2 + GameUser.userCars[listBoxCar.SelectedIndex].tuningLevel * 5000;
            price = price * GameUser.userCars[listBoxCar.SelectedIndex].damage / 100;
            if (GameUser.money < price)
            {
                MessageBox.Show("Brakuje Ci pięniędzy. Poszuka jakiejś pracy.", "Błąd!");
                return;
            }
            DialogResult result = MessageBox.Show("Czy na pewno chcesz naprawić wybrane auto za cenę " + price.ToString("C") + "?", "Potwierdzenie", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;
            context.UserCars.Where(c => c.ID == GameUser.userCars[listBoxCar.SelectedIndex].id).First().Damage = 0;
            context.Users.Where(u => u.ID == GameUser.userID).First().Money -= price;
            context.SubmitChanges();
            GameUser.Update(context);
            UpdateView();
        }

        #endregion

        #region Praca

        /// <summary>
        /// Wybór stanowiska pracy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxJobs.SelectedValue == null)
                return;
            labelJobName.Text = Job.allJobs[listBoxJobs.SelectedIndex].name;
            labelJobSalary.Text = "Wynagrodzenie: " + Job.allJobs[listBoxJobs.SelectedIndex].salary.ToString("C");
            labelJobDuration.Text = "Czas trwania: " + Job.allJobs[listBoxJobs.SelectedIndex].duration.ToString();
            labelJobEndTime.Text = "Przewidywane zakońćzenie: " + (DateTime.Now + Job.allJobs[listBoxJobs.SelectedIndex].duration).ToString();
        }

        /// <summary>
        /// Zatrudnienie do pracy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonJobAccept_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Czy na pewno chcesz rozpocząć pracę?\nZostaniesz wylogowany i nie będziesz mógł się zalogować zanim jej nie skończysz.", "Jesteś pewny?", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes)
                return;
            UserJob job = new UserJob()
            {
                userID = GameUser.userID,
                jobNumber = listBoxJobs.SelectedIndex,
                salary = Job.allJobs[listBoxJobs.SelectedIndex].salary,
                finishTime = DateTime.Now + Job.allJobs[listBoxJobs.SelectedIndex].duration
            };
            context.UserJobs.InsertOnSubmit(job);
            context.SubmitChanges();
            finish = false;
            MessageBox.Show("Rozpocząłeś pracę.", "Potwierdzenie");
            Close();
        }

        #endregion

        #region Wyścig

        /// <summary>
        /// Zablokowanie lub odblokowanie pola tekstowego wyboru przeciwnika
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxRandomRace_CheckedChanged(object sender, EventArgs e)
        {
            textBoxRaceOpponent.Enabled = !checkBoxRandomRace.Checked;
        }

        /// <summary>
        /// Obsługa wyścigu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRace_Click(object sender, EventArgs e)
        {
            bool raced;
            if (checkBoxRandomRace.Checked)
                raced = race.Go();
            else
                raced = race.Go(textBoxRaceOpponent.Text);
            if(raced)
            {
                GameUser.Update(context);
                UpdateView();
            }
        }

        #endregion
    }
}
