﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PRLracing.Model;
using PRLracing.View;

namespace PRLracing
{
    public partial class FormLogin : Form
    {
        /// <summary>
        /// Połączenie z bazą danych
        /// </summary>
        DatabaseDataContext context;

        /// <summary>
        /// Konstruktor domyślny
        /// </summary>
        public FormLogin()
        {
            InitializeComponent();
            context = new DatabaseDataContext();
        }

        /// <summary>
        /// Przycisk logowania
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            string query = string.Format("SELECT COUNT(*) FROM [Password] WHERE N='{0}' AND P='{1}'", textBoxLogin.Text, textBoxPassword.Text);
            if (context.ExecuteQuery<int>(query).First() < 1)
            {
                MessageBox.Show("Błąd przy logowaniu", "Błąd!");
                return;
            }

            GameUser.username = textBoxLogin.Text;
            GameUser.Update(context);

            FormMain form = new FormMain(context);
            Hide();
            form.ShowDialog();
            Close();
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            int counter = context.Users.Where(u => u.Name == textBoxLogin.Text).Count();
            if( counter > 0)
            {
                MessageBox.Show("Istnieje już użytkownik o takim loginie.", "Błąd");
                return;
            }
            context.Users.InsertOnSubmit(new User() {Name = textBoxLogin.Text, Money = 5000, Points = 0 });
            context.SubmitChanges();
            User user = context.Users.Where(u => u.Name == textBoxLogin.Text).FirstOrDefault();
            context.UserPasswords.InsertOnSubmit(new UserPassword() { userID = user.ID, Password = textBoxPassword.Text });
            context.UserCars.InsertOnSubmit(new UserCar() { Model = 0, User = user, TuningLevel = 0, Damage = 0 });
            context.SubmitChanges();
            MessageBox.Show("Dodano nowego użytkownika", "Powodzenie");
        }
    }
}
